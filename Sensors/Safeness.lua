local sensorInfo = {
	name = "Safeness",
	desc = "",
	author = "bentosming",
	date = "2019-04-23",
	license = "MIT"
}

local initialized = false
local map = {}
local enemies = {}
local mapSizeX
local mapSizeZ

--konstanty ke konfiguraci:

local granularity = 100
local rangeLimit = 1500
local gradientLimit = 300
local defaultRange = 400
local heightHillLimit = 80

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local GetGroundHeight = Spring.GetGroundHeight
local GetTeamUnits = Spring.GetTeamUnits

function getUnitInfo(id)
	x, y, z = Spring.GetUnitPosition(id)
	local info = {x = x, z = z, unitType = Spring.GetUnitDefID(id), range = defaultRange}
	if info.unitType ~= nil then
		weapon = UnitDefs[info.unitType].weapons
		info.range = 0
		for _, weapon in ipairs(weapon) do
			local weaponDefID = weapon["weaponDef"]
			local weaponDef = WeaponDefs[weaponDefID]
			if info.range < weaponDef.range then
				info.range = weaponDef.range + granularity / 2
			end
		end

		if info.range > rangeLimit then
			info.range = rangeLimit
		end
	end
	return info
end

function initialize()
	mapSizeX = Game.mapSizeX
	mapSizeZ = Game.mapSizeZ
	map["granularity"] = granularity

	for x = 0, mapSizeX, granularity do
		map[x] = {}
		for z = 0, mapSizeZ, granularity do
			map[x][z] = {x = x, z = z, safe = 1}
			if
				(math.abs(GetGroundHeight(x, z) - GetGroundHeight(x + granularity, z + granularity)) > gradientLimit or
					math.abs(GetGroundHeight(x + granularity, z) - GetGroundHeight(x, z + granularity)) > gradientLimit)
			 then
				map[x][z].safe = -1
			end
		end
	end

	local enemiesID = GetTeamUnits(1)
	for _, id in ipairs(enemiesID) do
		enemies[id] = getUnitInfo(id)
		map[f(enemies[id].x)][f(enemies[id].z)].safe = map[f(enemies[id].x)][f(enemies[id].z)].safe - 5
		updateMap(enemies[id])
	end

	initialized = true
end

function draw()
	if (Script.LuaUI("rect_update")) then
		Script.LuaUI.rect_update(
			123, -- key
			map
		)
	end
end

function update()
	for _, id in ipairs(GetTeamUnits(1)) do
		if enemies[id] == nill then
			enemies[id] = getUnitInfo(id)
			updateMap(enemies[id])
		end
		local newInfo = getUnitInfo(id)
		local oldInfo = enemies[id]
		--update jen pokud neco noveho
		if (newInfo.unitType ~= nil and (oldInfo.unitType == nil or (newInfo.x ~= oldInfo.x) or (newInfo.z ~= oldInfo.z))) then
			enemies[id] = newInfo
			updateMap(newInfo)
		end
	end
end

function f(x)
	return math.floor((x) / granularity) * granularity
end

function distance(ax, ay, az, bx, by, bz)
	return math.sqrt((ax - bx) * (ax - bx) + (az - bz) * (az - bz) + (ay - by) * (ay - by))
end

function updateMap(info)
	if info.range then
		local posX = f(info.x)
		local posZ = f(info.z)
		local posY = GetGroundHeight(info.x, info.z)
		map[posX][posZ].safe = map[posX][posZ].safe - 5
		for x = f(info.x - info.range), f(info.x + info.range), granularity do
			for z = f(info.z - info.range), f(info.z + info.range), granularity do
				y = GetGroundHeight(x, z)
				if
					(distance(x, y, z, info.x, posY, info.z) < info.range) and (x >= 0) and (z >= 0) and (x <= mapSizeX) and
						(z <= mapSizeZ)
				 then
					--Jednoduchy "raycast"
					local hillInTheWay = false
					for t = 0.01, 0.99, 0.1 do
						xx = (posX - x) * t + x
						zz = (posZ - z) * t + z
						yy = (posY - y) * t + y
						if GetGroundHeight(xx, zz) > yy + heightHillLimit then
							map["hill"] = {x = x, z = z, xx = xx, yy = yy, zz = zz, yyy = GetGroundHeight(xx, zz)}
							hillInTheWay = true
						end
					end
					if not hillInTheWay then
						map[x][z].safe = map[x][z].safe - 2
						-- map[x][z].safeReason = info.x .. info.z
					end
				end
			end
		end
	end
end

-- @description updates information about safeness on map
return function()
	if (not initialized) then
		initialize()
	else
		--draw()
		update()
	end

	return map
end
