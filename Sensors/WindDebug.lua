local sensorInfo = {
	name = "ExampleDebug",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT"
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return current wind statistics
return function(wind)
	if #units > 0 then
		local unitID = units[1]
		if (Script.LuaUI("windDebug_update")) then
			Script.LuaUI.windDebug_update(
				unitID, -- key
				Sensors.map.Wind
			)
			return {}
		end
	end
end
