local sensorInfo = {
	name = "GetPositionsAlongEdges",
	desc = "",
	author = "bentosming",
	date = "2019-04-23",
	license = "MIT"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local GetGroundHeight = Spring.GetGroundHeight
local GetTeamUnits = Spring.GetTeamUnits


-- @description gets positions along east, west and south edges of map

return function(unitCount)
	local unitPerSide = unitCount / 3
	local maxX = Game.mapSizeX
	local maxZ = Game.mapSizeZ
	local positions = {}
	local j = 1
	for x = 1, unitPerSide do
		positions[j] = Vec3(maxX / unitPerSide * x, 100, maxZ)
		j = j + 1
	end

	for z = 1, unitPerSide do
		positions[j] = Vec3(0, 100, maxZ / unitPerSide * z)
		j = j + 1
		positions[j] = Vec3(maxX, 100, maxZ / unitPerSide * z)
		j = j + 1
	end

	return positions
	-- return
end
