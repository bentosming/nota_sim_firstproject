local sensorInfo = {
	name = "GetGroundAllyUnits",
	desc = "Returns ground allied units, that are not in safe area.",
	author = "bentosming",
	date = "2017-05-30",
	license = "notAlicense"
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Returns ground allied units, that are not in safe area.
-- @argument mi [table] - mission info of ttdr mission
-- @return newListOfUnits [array of units - id, position, dist] ground allied units, that are not in safe area.
return function(mi)
	local listOfUnits = Spring.GetTeamUnits(0)
	local safeArea = mi.safeArea
	local safeAreaVec3 = Vec3(safeArea.center.x, safeArea.center.y, safeArea.center.z)
	local newListOfUnits = {}

	listOfUnits = Sensors.core.FilterUnitsByCategory(listOfUnits, Categories.Common.groundUnits)

	for i = 1, #listOfUnits do
		local thisUnitID = listOfUnits[i]
		local thisUnitDefID = Spring.GetUnitDefID(thisUnitID)
		unitPos = Vec3(Spring.GetUnitPosition(thisUnitID))
		dist = unitPos:Distance(safeAreaVec3)
		if (dist > safeArea.radius*0.7) then
			newListOfUnits[#newListOfUnits + 1] = {position = unitPos, id = thisUnitID, distance = dist}
		end
	end

	return newListOfUnits
end
