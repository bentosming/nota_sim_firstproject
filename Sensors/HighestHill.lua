local sensorInfo = {
	name = "Hills",
	desc = "Returns hills ordered from closest to furthest ",
	author = "bentosming",
	date = "2019-04-23",
	license = "MIT"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local GetGroundHeight = Spring.GetGroundHeight

-- @description Returns hills ordered from closest to furthest, pos should be the "center" and shouldn't be on a hill
return function()
	local maxX = Game.mapSizeX
	local maxZ = Game.mapSizeZ
	local step = 1
	local maxHeight = -100
	local maxPosition = Vec3(0, 0, 0)

	for x = 0, maxX, step do
		for z = 0, maxZ, step do
			local y = GetGroundHeight(x, z)
			local xyz = Vec3(x, y, z)
			if (maxHeight < y) then
				maxHeight = y
				maxPosition = Vec3(x, y, z)
				local i = 1
			end
		end
	end
	--
	--
	return maxPosition
end
