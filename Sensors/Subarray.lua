local sensorInfo = {
	name = "Subarray",
	desc = "Returns hills ordered from closest to furthest ",
	author = "bentosming",
	date = "2018-04-16",
	license = "MIT"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local GetGroundHeight = Spring.GetGroundHeight

-- @description
return function(array, first, last)
	local sub = {}
	for i = first, last do
		sub[#sub + 1] = array[i]
	end
	return sub
end
