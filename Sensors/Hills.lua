local sensorInfo = {
	name = "Hills",
	desc = "Returns hills ordered from closest to furthest ",
	author = "bentosming",
	date = "2019-04-23",
	license = "MIT"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local GetGroundHeight = Spring.GetGroundHeight

-- @description Returns hills ordered from closest to furthest, pos should be the "center" and shouldn't be on a hill
return function(pos, hillHeight)
	positon = Vec3(pos.x, pos.y, pos.z)
	local maxX = Game.mapSizeX
	local maxZ = Game.mapSizeZ
	local step = 30
	local maxStep = 300
	local hills = {}

	for x = 0, maxX, step do
		for z = 0, maxZ, step do
			local y = GetGroundHeight(x, z)
			local xyz = Vec3(x, y, z)
			if (hillHeight == y) then
				local i = 1
				while (i < #hills + 1 and hills[i]:Distance(xyz) > maxStep) do
					i = i + 1
				end

				local hc = #hills + 1
				if (i == hc) then
					-- add new hill in sorted array
					for j = 1, hc - 1, 1 do
						local d2 = xyz:Distance(positon)
						local d1 = hills[j]:Distance(positon)
						if (d1 > d2) then
							hills[j], xyz = xyz, hills[j]
						end
					end

					hills[#hills + 1] = xyz
				end
			end
		end
	end
	--
	--
	return hills
end
