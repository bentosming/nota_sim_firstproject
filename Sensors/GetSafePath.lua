local sensorInfo = {
	name = "GetSafePath",
	desc = "",
	author = "bentosming",
	date = "2019-04-23",
	license = "MIT"
}

local granularity

local map = {}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local GetGroundHeight = Spring.GetGroundHeight
local GetTeamUnits = Spring.GetTeamUnits

-- - - - -
-- queue implementation
function GetQueue()
	local queue = {}
	queue.start = 1
	queue.endIndex = 1
	return queue
end

function pop(q)
	local temp = q[q.start]
	q[q.start] = nil
	q.start = q.start + 1
	return temp
end

function push(q, val)
	q[q.endIndex] = val
	q.endIndex = q.endIndex + 1
end
-- end of queue implementation
-- - - - -

function BFS(startPos, goalPos)
	local queue = GetQueue()

	push(queue, startPos)
	local visited = {}
	visited[startPos.x .. startPos.z] = "start"
	local active = pop(queue)
	while active and not (active.x == goalPos.x and active.z == goalPos.z) do
		local neighbours = neighbours(active)
		for i = 0, 8, 1 do
			n = neighbours[i]
			if (n and not visited[n.x .. n.z]) then
				if (n.safe >= 0) then
					push(queue, neighbours[i])
					visited[n.x .. n.z] = active
				end
			end
		end

		active = pop(queue)
	end

	path = {}
	x, z = goalPos.x, goalPos.z
	i = 2
	path[i] = {x = x, z = z}
	while visited[x .. z] ~= "start" and visited[x .. z] ~= nil do
		i = i + 1
		path[i] = {x = visited[x .. z].x, z = visited[x .. z].z, safe = map[x][z].safe}
		--Spring.Echo(path[i].x .. "  " .. path[i].z .. " " .. path[i].safe)
		x, z = visited[x .. z].x, visited[x .. z].z
	end
	return path
end

function neighbours(info)
	n = {}

	-- direct
	if map[info.x - map.granularity] and map[info.x - map.granularity][info.z] then
		n[0] = map[info.x - map.granularity][info.z]
	end
	if map[info.x + map.granularity] and map[info.x + map.granularity][info.z] then
		n[1] = map[info.x + map.granularity][info.z]
	end
	if map[info.x] and map[info.x][info.z - map.granularity] then
		n[2] = map[info.x][info.z - map.granularity]
	end
	if map[info.x] and map[info.x][info.z + map.granularity] then
		n[3] = map[info.x][info.z + map.granularity]
	end

	--diagonal
	if map[info.x - map.granularity] and map[info.x - map.granularity][info.z - map.granularity] then
		n[4] = map[info.x - map.granularity][info.z - map.granularity]
	end
	if map[info.x + map.granularity] and map[info.x + map.granularity][info.z - map.granularity] then
		n[5] = map[info.x + map.granularity][info.z - map.granularity]
	end
	if map[info.x - map.granularity] and map[info.x - map.granularity][info.z + map.granularity] then
		n[6] = map[info.x - map.granularity][info.z + map.granularity]
	end
	if map[info.x + map.granularity] and map[info.x + map.granularity][info.z + map.granularity] then
		n[7] = map[info.x + map.granularity][info.z + map.granularity]
	end

	-- if map[info.x-2*map.granularity] and map[info.x-2*map.granularity][info.z] then
	--	n[8] = map[info.x-2*map.granularity][info.z]
	-- end
	-- if map[info.x+2*map.granularity] and map[info.x+2*map.granularity][info.z] then
	--	n[9] = map[info.x+2*map.granularity][info.z]
	-- end
	return n
end

local granularity = 1

function f(x)
	local ff = math.floor((x) / granularity) * granularity
	if ff < 0 then
		return 0
	else
		return ff
	end
end

-- @description returns safe path from unit to goalPos based on mapInput

return function(mapInput, unitId, goalPos)
	map = mapInput

	local x, y, z = Spring.GetUnitPosition(unitId)
	granularity = map.granularity
	local path = BFS({x = f(x), z = f(z), safe = 1}, {x = f(goalPos.x), z = f(goalPos.z), safe = 1})
	path[1] = {x = goalPos.x, z = goalPos.z}
	return path
end
