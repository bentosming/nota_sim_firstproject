local sensorInfo = {
	name = "GetWeaponRange",
	desc = "",
	author = "bentosming",
	date = "2019-04-23",
	license = "MIT"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local GetGroundHeight = Spring.GetGroundHeight
-- @description returns weapon range of unit
return function(unitID)
	unitDefID = Spring.GetUnitDefID(tonumber(unitID))
	weapons = UnitDefs[unitDefID].weapons
	sweaponDefID = Spring.GetWeaponDefID(weapons[1])
	range = WeaponDefs[weaponDefID].range

	return range
end
