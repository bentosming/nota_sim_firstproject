local sensorInfo = {
	name = "GetUnitsXZDistance",
	desc = "Returns distance of two units in XZ plane",
	author = "bentosming",
	date = "2017-05-30",
	license = "notAlicense"
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return filtered list of units
-- @argument unitAId [int]
-- @argument unitBId [int]
return function(unitAId, unitBId)
	local ax, ay, az = Spring.GetUnitPosition(unitAId)
	local bx, by, bz = Spring.GetUnitPosition(unitBId)
	return math.sqrt((ax - bx) * (ax - bx) + (az - bz) * (az - bz))
end
