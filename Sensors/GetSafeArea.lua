local sensorInfo = {
	name = "GetGroundAllyUnits",
	desc = "Filter list of units by category.",
	author = "PepeAmpere",
	date = "2017-05-30",
	license = "notAlicense"
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description returns random place within safe area

function f(x)
	return math.floor((x) / 15) * 15
end

return function(mi)
	local safeArea = mi.safeArea

	local a = math.random() * 2 * math.pi
	local r = 0.8*safeArea.radius * math.sqrt(math.random())

	return Vec3(f(safeArea.center.x + r * math.cos(a)), safeArea.center.y, f(safeArea.center.z + r * math.sin(a)))
end
